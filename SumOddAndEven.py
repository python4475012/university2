'''
def sum_odd_and_even(a):
	s0, s1 = 0, 0
	for i in range(len(a)):
		if a[i] % 2 == 0:
			s0 += a[i]
		else:
			s1 += a[i]
	return [s0, s1]

def sum_odd_and_even(a):
	s0, s1 = 0, 0
	for x in (a):
		if x % 2 == 0:
			s0 += x
		else:
			s1 += x
	return [s0, s1]
'''
def sum_odd_and_even(a):
    s0 = sum(x for x in a if x % 2 == 0)  # 總和偶數
    s1 = sum(a) - s0  # 總和減去偶數就是奇數的總和
    return [s0, s1]

a = [1, 2, 3, 4, 5, 6]
print(sum_odd_and_even(a))
