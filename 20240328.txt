20240328

(1)Dictionaries	*很重要
資料庫 透過關鍵字找答案


car = {
"brand": "Ford",
"model": "Mustang",
"year": 1964
}

x = car.keys()

print(x) #before the change

car["color"] = "white"

print(x) #after the change

>

dict_keys(['brand', 'model', 'year'])
dict_keys(['brand', 'model', 'year', 'color'])


a = [4,3,2,5]
b = [4,10,2,6,7,8,9]
a >= b
False

a = [2,3,4,1]
int(''.join(str(x) for x in a))

 ''.join(2*x for x in txt)

def XO(txt):
	txt = txt.lower()
	return txt.count('x') == txt.count('o')