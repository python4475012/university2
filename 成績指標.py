a=int(input())      #輸入人數
b=input()           #輸入成績
list=b.split()      #將成績轉為清單
list.sort()         #將清單中成績排序(由小到大)

#以下為第一行=======================================
print(' '.join(list))   #將排序好的清單轉為字串並打印

#以下為第二行=======================================
list.reverse()              #將清單順序反轉(由大到小)

for i in range(0,a):        
    if int(list[i])<60:     #如果找到第一個小於60的數,打印並break
        print(list[i])
        break
if int(list[a-1])>=60:      #如果清單中最小數>=60,就代表所有人及格
    print('best case')

#以下為第三行=======================================
list.reverse()              #將清單順序反轉(由小到大)

for j in range(0,a):        
    if int(list[j])>=60:    #如果找到第一個大於60的數,打印並break
        print(list[j])
        break
if int(list[a-1])<60:       #如果清單中最大數<60,就代表所有人都不及格
    print('worst case')
