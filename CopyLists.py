a = ["apple", "banana", "cherry"]
b = a
# b = a.copy() 等於 b = a[::] 等於 b = list(a)
#和 b = a 的資料處理不同
print(b)
print(a)

# b[0] = 'orange'
# print(b)
# print(a)

print(id(a))
print(id(a))

print(a == b)
print(a is b)